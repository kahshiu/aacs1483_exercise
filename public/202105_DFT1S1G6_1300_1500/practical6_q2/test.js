// variable: operation on numbers
var num1 = 1;
var num2 = 2;
var num3 = num1 + num2

function adding(n1, n2) {
	return n1 + n2
}

var num4 = adding(20, 3)
num4
/* */

// variable: operation on strings 
var username = "Chin Jun"
var greeting = "Hello, "
var greetUser = greeting + username; // string concantenation


function mmWord(mm) {
	var storeWords = [
		/*index: 0*/ "January",
		/*index: 1*/ "February",
		/*index: 2*/ "March",
		/*index: 3*/ "April",
		/*index: 4*/ "May",
		/*index: 5*/ "June",
		/*index: 6*/ "July",
		/*index: 7*/ "August",
		/*index: 8*/ "September",
		/*index: 9*/ "October",
		/*index: 10*/ "November",
		/*index: 11*/ "December",
	]
	return storeWords[mm];
}
/*
0 Jan
1 Feb 
2 Mac 
3 Apr 
4 May 
5 Jun 
6 Jul 
7 Aug <--- august is indexed 7 
*/
function formatDate(dt) {
	var yyyy = dt.getFullYear()
	var mm = dt.getMonth() + 1
	var dd = dt.getDate();

	return dd + "/" + mm + "/" + yyyy // string concatenation
}

function formatDate2(dt) {
	var yyyy = dt.getFullYear()
	var mm = dt.getMonth()
	var dd = dt.getDate();

	return mmWord(mm) + " " + dd + ", " + yyyy // string concatenation
}

function formatTime(dt) {
	var hh = dt.getHours()
	var mi = dt.getMinutes();
	var ss = dt.getSeconds();
	return hh.toString().padStart(2, 0)
		+ ":" + mi.toString().padStart(2, 0)
		+ ":" + ss.toString().padStart(2, 0)
}

function formatDateTime(dt) {
	var dtString = formatDate(dt) + " " + formatTime(dt); // string concatenation
	return dtString;
}

function nextYear(dt) {
	var yyyy = dt.getFullYear()
	return yyyy + 1;
}

function diffDays(dt1, dt2) {
	var diffms = dt2.getTime() - dt1.getTime()
	var dayms = 1000 * 60 * 60 * 24;
	return Math.ceil(diffms / dayms);
}

function htmlYear(dt) {
	var yyyy2 = nextYear(dt);
	var htmlstring = "<h2>Countdown to New Year " + yyyy2 + "</h2>"
	return htmlstring
}

function htmlToday(dt) {
	var dd = formatDate2(dt);
	var htmlstring = "<p>Today is " + dd + "</p>"
	return htmlstring
}

var d1 = new Date(2021, 4, 1)
var d2 = new Date(nextYear(d1), 0, 1)

var dd = diffDays(d1, d2)

function htmlDaysLeft(dt) {
	var yyyy2 = nextYear(dt);
	var d1 = dt;
	var d2 = new Date(yyyy2, 0, 1);
	var left = diffDays(d1, d2);
	var htmlstring = "<p>Only " + left + " days left until New Year " + yyyy2 + "</p>"

	var timestr = formatTime(dt);
	var htmlstring2 = "<p>" + timestr + "</p>"
	return htmlstring + htmlstring2
}





// s1
// s2
// s3
// 
// // var dd = formatDateTime(dt)
// var yyyyNext = nextYear(d1)
// var ww = mmWord(d1.getMonth());
// ww
// 
// yyyyNext
// greetUser